	
<?php /* Template Name: NewPost */ 

get_header(); ?>
	<div class="container">
		<div class="row">
			<div id="primary" class="col-md-9 content-area">
				<main id="main" class="site-main" role="main">
					<h1 class="new_post_heading">Add New Post</h1>
					<form role="form">
						<?php 
							$args = array('hide_empty' => 0);
							$categories = get_categories($args);
							foreach ($categories as $category) {
								$label = '<label for="'.$category->category_nicename.'">'.$category->cat_name.'</label>';
								$checkbox = '<input type="checkbox" class="checkbox_category" name="'.$category->category_nicename.'" value="'.$category->cat_ID.'" id="'.$category->category_nicename.'" />';
								echo $label.$checkbox;
							}
						?>
					<div class="form-group">
						<label for="postHeading">Heading</label>
						<input type="text" class="form-control" id="postHeading" placeholder="Enter heading here"/>
					</div>
					<div class="form-group">
						<label for="post_text">Content</label>
						<?php 
							$settings = array('media_buttons' => true);
							$content = '';
							$editor_id = 'myCustomEditor';
							wp_editor($content, $editor_id, $settings);
						?>
						<input type="button" value="Publish" class="publish_article" />
					</div>
					<input type="hidden" value="<?php echo wp_create_nonce("lyh_blog_new_post"); ?>" id="lyh_new_post_nonce" />
				</form>
				</main><!-- #main -->
			</div><!-- #primary -->

			<?php get_sidebar( 'sidebar-1' ); ?>
		</div> <!--.row-->
	</div><!--.container-->
	<?php get_footer(); ?>
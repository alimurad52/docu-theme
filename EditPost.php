	
<?php /* Template Name: EditPost */ 

get_header(); ?>
	<div class="container">
		<div class="row">
			<div id="primary" class="col-md-9 content-area">
				<main id="main" class="site-main" role="main">
					<h1 class="new_post_heading">Edit Post</h1>
					<form role="form">
						<?php 
							$oldpost = get_post(get_query_var("postid"));
							var_dump(wp_get_post_categories($oldpost->ID));
							$args = array('hide_empty' => 0);
							$categories = get_categories($args);
							foreach ($categories as $category) {
								$label = '<label for="'.$category->category_nicename.'">'.$category->cat_name.'</label>';
								$checkbox = '<input type="checkbox" class="checkbox_category" name="'.$category->category_nicename.'" value="'.$category->cat_ID.'" id="'.$category->category_nicename.'" />';
								echo $label.$checkbox;
							}
							$settings = array("media_buttons" => true);
							$content = $oldpost->post_content;
							$title = $oldpost->post_title;
							$editor_id = "myCustomEditor";
							echo '<div class="form-group"><label for="postHeading">Heading</label><input type="text" class="form-control" id="postHeading" placeholder="Enter heading here" value="'.$title.'" /></div><div class="form-group"><label for="post_text">Content</label>.'.wp_editor($content, $editor_id, $settings).'<input type="button" value="Publish" class="publish_article" /></div>';
						?>
					<input type="hidden" value="<?php echo wp_create_nonce("lyh_blog_new_post"); ?>" id="lyh_new_post_nonce" />
				</form>
				</main><!-- #main -->
			</div><!-- #primary -->

			<?php get_sidebar( 'sidebar-1' ); ?>
		</div> <!--.row-->
	</div><!--.container-->
	<?php get_footer(); ?>
jQuery(document).ready(function($) {
	$('.publish_article').on("click", function(e) {
		console.log(tinymce.editors.myCustomEditor.getContent());
		var lyh_blog_content = tinymce.editors.myCustomEditor.getContent();
		var lyh_blog_title = $('#postHeading').val();
		var array_checkbox = [];
		var lyh_blog_nonce_new_post = $('#lyh_new_post_nonce').val();
		$('.checkbox_category').each(function() {
			if($(this).is(":checked")) {
				array_checkbox.push($(this).val());
			}
		})
		console.log(array_checkbox);
		$.ajax({
			type: 'POST',
			url: ajax_url,
			data: {
				"action": "lyh_savepost",
				"content": lyh_blog_content,
				"title": lyh_blog_title,
				"categories": array_checkbox,
				"nonce": lyh_blog_nonce_new_post
			},
			success: function(data){
				console.log(data);
			}
		});
	})
})
<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_script('blog-script', get_stylesheet_directory_uri() . '/dist/js/scripts.min.js', null, true );
}
add_filter('wp_head','add_tinymce_editor');

add_action( 'wp_ajax_nopriv_lyh_savepost', 'lyh_blog_ajax_save_post' );
add_action( 'wp_ajax_lyh_savepost', 'lyh_blog_ajax_save_post' );

add_action('init', 'dcc_rewrite_tags');
function dcc_rewrite_tags() {
    add_rewrite_tag('%postid%', '([^&]+)');
}

add_action('init', 'dcc_rewrite_rules');
function dcc_rewrite_rules() {
    add_rewrite_rule('^edit-post/(.+)/?$','index.php?pagename=edit-post&postid=$matches[1]','top');
}

function lyh_blog_ajax_save_post() {
	$args = array(
    	"post_status" => "publish",
    	"post_author" => get_current_user_id(),
    	"post_content" => $_POST['content'],
    	"post_title" => $_POST['title']
	);
	$postId = wp_insert_post($args);
	wp_set_post_categories($postId, $_POST["categories"]);
	echo $postId;
	var_dump(get_theme_root()) ;
	wp_die();
}

?>
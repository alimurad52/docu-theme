<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package LyrehouseDocumentation
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'nisarg' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'nisarg' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'nisarg' ); ?>" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'nisarg' ); ?></span></button>
</form>
<form>
	<?php 
		$user = wp_get_current_user();
		$allowed_roles = array('editor', 'administrator', 'author');
		if(array_intersect($allowed_roles, $user->roles)) {
			$location = get_site_url() . "/?page_id=42";
			echo '<a href='.$location.'><input type="button" class="new_post" value="New Post"></a>';
		}
	?>
</form>
